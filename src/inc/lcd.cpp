#include "helpers.h"
#include <LiquidCrystal_I2C.h>

const int lcd_width = 16;
const int lcd_height = 2;

LiquidCrystal_I2C lcd(0x3f, lcd_width, lcd_height);          //    Задава се LCD адреса 0x27 , 16 символа ( знака ), 2 реда

void lcd_write_at (char c, int x, int y) {
  lcd.setCursor(x, y);
  lcd.write(c);
}

void lcd_prn (const char c[], int x, int y)
{
  for(int i = 0; i < strlen(c) && (x + i) < lcd_width; i++) {
    lcd_write_at(c[i], x + i, y);
  }
}

void lcd_init () {
  lcd.init();                            //     Инициализация на LCD
  lcd.backlight();

  // lcd.load_custom_character(1, char_box);
  // lcd.load_custom_character(2, char_box_filled);
  // lcd.load_custom_character(3, char_box_arrow);
  // lcd.load_custom_character(4, char_box_filled_arrow);
  // //
  // lcd.load_custom_character(5, arr_up);
  // lcd.load_custom_character(6, arr_down);
  // lcd.load_custom_character(7, arr_up_down);

  // delay(100);
  lcd.clear();                         //     Изстриване на екрана
  // delay(100);
  lcd.setCursor(0, 0);
  // lcd.write(7);
  // prn(lcd, " 0   140 4/4 ", 1, 0);        //     На дисплея се изписва текста ROBOTEV
  lcd_prn("Channel  1", 0, 0);        //     На дисплея се изписва текста ROBOTEV

  // lcd.setCursor(2, 0);
  // lcd.cursor_on();
}


// copies the contents of character a into b
void cpy_char (char a[], char b[]);

// puts an int inside a character
void num_left_pad(char a[], int d);

// toggles int between 0 and 1
void toggle_int (int *toggleVal);
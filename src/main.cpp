#include <Arduino.h>
#include <Wire.h>
#include <MIDI.h>
#include <LiquidCrystal_I2C.h>
#include "inc/helpers.h"
#include "inc/lcd.h"


static const int PIN_CHANNEL_POT = 0;

MIDI_CREATE_DEFAULT_INSTANCE();

int switch_channel = 1;
int channel = 0;

void setup()
{
  MIDI.begin(MIDI_CHANNEL_OMNI);
  MIDI.turnThruOff();
  lcd_init();
}

void loop()
{
  uint32_t channel_pot_val;
  char chan_val_str[2];
  int new_channel = channel;
  channel_pot_val = analogRead(PIN_CHANNEL_POT);
  new_channel = int(((float(channel_pot_val) / 1024) * 16));
  if (new_channel != channel) {
    channel = new_channel;
    if (new_channel >= 9) {
      snprintf(chan_val_str, 4, "%d", new_channel + 1);
    } else {
      snprintf(chan_val_str, 4, " %d", new_channel + 1);
    }
    // num_left_pad(chan_val_str, new_channel); 
    lcd_prn(chan_val_str, 8, 0);
    delay(100);
  }

  if (MIDI.read())
    {
      // switch the channel
      MIDI.send(MIDI.getType(),
        MIDI.getData1(),
        MIDI.getData2(),
        channel + 1);
    }

}